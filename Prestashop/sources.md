# Prestashop Sources

* [Releases · PrestaShop\/PrestaShop](https://github.com/PrestaShop/PrestaShop/releases?page=1 "Releases · PrestaShop/PrestaShop")
* [Optimize your PrestaShop \:\: PrestaShop Developer Documentation](https://devdocs.prestashop-project.org/8/scale/optimizations/#5-mysqlmariadb-settings "Optimize your PrestaShop :: PrestaShop Developer Documentation")
* [Showing results for \'backups\'\. \- PrestaShop Forums](https://www.prestashop.com/forums/search/?q=backups&quick=1 "Showing results for \'backups\'. - PrestaShop Forums")
